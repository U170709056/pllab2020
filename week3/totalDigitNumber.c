#include <stdio.h>

int nrDigits(int number){
    
    static int count = 0;
    if (number > 0)
    {
        count++;
        nrDigits(number / 10);
    }
    else 
    {
        return count;
    }
}

int main()
{
    int number;
    int count = 0;
    
    printf("Enter an integer number: ");
    scanf("%d", &number);
    count  = nrDigits(number);
    printf("Total digits number is: %d\n", count);
    return 0;
}