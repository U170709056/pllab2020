#include <stdio.h>
#include <math.h>

FILE *outp;

float arb_func(float x)
{
    return x * log10(x) - 1.2;
}

void rf(float *x, float x0, float x1, float fx0, float fx1, int *itr)
{

    *x = ((x0 * fx1) - (x1 * fx0)) / (fx1 - fx0);
    ++(*itr);
    printf("Iteration %d: %.5f\n", *itr, *x);
    fprintf(outp, "Iteration %d: %.5f\n", *itr, *x);
}

int main()
{

    int itr, maximtr;
    float x, x0, x1, fx0, fx1, x_curr, x_next, error;
    outp = fopen("rf.txt", "w");


    printf("Enter interval values [x0, x1], allowed error and number of iterations: \n");
    scanf("%f %f %f %d", &x0, &x1, &error, &maximtr);

    rf(&x_curr, x0, x1, arb_func(x0), arb_func(x1), &itr);

    do
    {

        if (arb_func(x0) * arb_func(x_curr) < 0)
        {
            x1 = x_curr;
        }
        else
        {
            x0 = x_curr;
        }

        rf(&x_next, x0, x1, arb_func(x0), arb_func(x1), &itr);
        if (fabs(x_next - x_curr) < error)
        {
            printf("After %d Iterations, root = %.5f\n", itr, x_next);
            fprintf(outp, "After %d Iterations, root = %.5f\n", itr, x_next);
            return 0;
        }
        else
        {
            x_curr = x_next;
        }
    }while (itr < maximtr);

    fclose(outp);
    return 1;
}
